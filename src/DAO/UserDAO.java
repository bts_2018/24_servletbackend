package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import Entity.User;

public class UserDAO {

	private Connection conn = null;
	private Statement stmt = null;

	//Constructor
	public UserDAO()  {
		try {
			//Register Driver
			java.sql.DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			
			//Connection
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost/ecommerce?user=root&password=");

			//Create Statement
			this.stmt =  conn.createStatement();

		}catch(Exception ex) {
			System.out.println("error on UserDAO constructor : " + ex.getMessage());
		}
	}


	//CRUD
	public int CreateUser(User user) {

		int newid=0;

		try {
			String sql = "INSERT INTO users (nameuser,birthday,country,username,email) VALUES ('" 
					+ user.getNameuser() + "','" 
					+ user.getBirthday() + "','" 
					+ user.getCountry() + "','" 
					+ user.getUsername() + "','"
					+ user.getEmail() + "')";

			System.out.println(sql);

			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected  > 0) {
				sql = "SELECT MAX(idUser) as idUser FROM users";

				//Execute query
				ResultSet rs  = this.stmt.executeQuery(sql);
				if (rs.next()) {
					newid = rs.getInt("idUser");
				}else {
					throw new Exception("Error to INSERT data");
				}
			}

			return newid;

		}catch(Exception ex){
			System.out.println("error on CreateUser: " + ex.getMessage());
			return 0;
		}
	}


}
