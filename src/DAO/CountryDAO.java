package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import Entity.Country;

public class CountryDAO extends Country {
	
	private Connection conn = null;
	private Statement stmt = null;
	
	//Constructor
	public CountryDAO()  {
		try {
			
			//Register Driver
			java.sql.DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			
			//Connection
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost/ecommerce?user=root&password=");

			//Create Statement
			this.stmt =  conn.createStatement();

		}catch(Exception ex) {
			System.out.println("error on CountryDAO constructor  : " + ex.getMessage());
		}
	}

	
	public ArrayList<Country> GetCountries() {

		try {

			//SQL
			String sql = "SELECT * FROM country";

			//Execute query
			ResultSet rs  = this.stmt.executeQuery(sql);

			ArrayList<Country> categories = new ArrayList<Country>();

			while (rs.next()) {
				Country item = new Country();
				item.setCode(rs.getString("code"));
				item.setName(rs.getString("name"));

				categories.add(item);
			}

			return categories;

		}catch (Exception ex) {
			System.out.println("error on GetCountries  : " + ex.getMessage());
			return null;
		}

	}

	
}
