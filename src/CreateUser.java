

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import DAO.UserDAO;
import Entity.Country;
import Entity.User;

 
@WebServlet("/CreateUser")
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public CreateUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	 
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			
			//Permissions
			response.addHeader("Access-Control-Allow-Origin", "*");
			
			
			//GET PARAMETERS FROM AJAX
			String nameuser = request.getParameter("nameuser");
			String birthday = request.getParameter("birthday");
			String country = request.getParameter("country");
			String username = request.getParameter("username");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			 
			// MODEL 
			User user = new User();
			user.setNameuser(nameuser);
			user.setBirthday(birthday);
			user.setCountry(country);
			user.setUsername(username);
			user.setEmail(email);
			user.setPassword(password);
			
			// DAO
			UserDAO DAO = new UserDAO();
			int idUser = DAO.CreateUser(user);
			

			// JSON
			JSONArray list = new JSONArray();
			JSONObject item = new JSONObject();
			item.put("idUser", idUser);
			list.add(item);  
			
			//OUTPUT
			response.getWriter().append(list.toJSONString());
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		
	}

	 

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		doGet(request, response);
	}

}
