package Entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User {
			
	//ATTRIBUTES
	private int IdUser;
	private String nameuser;
	private String birthday;
	private String country;
	private String username;
	private String email;
	private String password;
	
	//METHOD
	public int getIdUser() {
		return IdUser;
	}
	public void setIdUser(int idUser) {
		IdUser = idUser;
	}
	public String getNameuser() {
		return nameuser;
	}
	public void setNameuser(String nameuser) {
		this.nameuser = nameuser;
	}
	public String getBirthday() {
		
		Date x;
		String y= null;
		
		try {
			//
			x = new SimpleDateFormat("MM/dd/yyyy").parse(this.birthday);
			
			y = new SimpleDateFormat("yyyyMMdd").format(x);
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());	
		}
		
		return y;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

	
}
